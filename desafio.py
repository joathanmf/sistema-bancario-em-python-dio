menu = """

[d] Depositar
[s] Sacar
[e] Extrato
[q] Sair

=> """

saldo = 0
limite = 500
extrato = ""
numero_saques = 0
LIMITE_SAQUES = 3

while True:
  opcao = input(menu)

  if opcao == "d":
    valor = float(input("Informe o valor do depósito: "))

    if valor <= 0:
      print("Valor inválido, por favor informe um valor maior que zero.")
      continue
    
    saldo += valor
    extrato += f"Depósito: R${valor:.2f}\n"
    print("Depósito realizado com sucesso.")

  elif opcao == "s":
    if numero_saques >= LIMITE_SAQUES:
      print("Limite de saques atingido.")
      continue

    valor = float(input("Informe o valor do saque: "))

    if valor <= 0:
      print("Valor inválido, por favor informe um valor maior que zero.")
      continue

    if saldo < valor:
      print("Saldo insuficiente.")
      continue

    if valor > limite:
      print("Valor acima do limite permitido.")
      continue

    saldo -= valor
    extrato += f"Saque: R${valor:.2f}\n"
    numero_saques += 1
    print("Saque realizado com sucesso.")

  elif opcao == "e":
    print(f"Saldo: R${saldo:.2f}\n")
    print(extrato)

  elif opcao == "q":
    print("Saindo do sistema...")
    break

  else:
    print("Operação inválida, por favor selecione novamente a operação desejada.")